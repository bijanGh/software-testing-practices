"use strict"

window.onload = function () {
  const firstNameEl = document.getElementById('firstName')
  const lastNameEl = document.getElementById('lastName')
  const submitEl = document.getElementById('submit')
  const listEl = document.getElementById('list')
  let firstName = ''
  let lastName = ''

  if (firstNameEl !== null) {
    firstNameEl.onchange = (e: Event): void => {
      const target = e.target as HTMLInputElement
      firstName = target.value
    }
  }

  if (lastNameEl !== null) {
    lastNameEl.onchange = (e: Event): void => {
      const target = e.target as HTMLInputElement
      lastName = target.value
    }
  }
  const makeFullName = (first: string, last: string): HTMLElement => {
    const li = document.createElement('li')
    const text = document.createTextNode(`${first} ${last}`)
    li.appendChild(text)
    return li
  }
  if (submitEl !== null) {
    submitEl.onclick = () => {
      if (listEl !== null) { listEl.appendChild(makeFullName(firstName, lastName)) }
    }
  }
}
