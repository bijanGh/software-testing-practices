import { fact } from './helpers'

describe('math', () => {
  it('it should return 6 if  we pass 3', () => {
    expect(fact(3)).toBe(6)
  })
  it('it should return false if we', () => {
    expect(fact(-3)).toBe(false)
  })
  it('it should return NaN', () => {
    expect(fact(0.1)).toBe(NaN)
  })
})
