export const helloWorld = (): string => 'hello world'

export function testRunner(fn: any, result: any): boolean {
  return fn() === result
}

console.log(testRunner(helloWorld, 'hello world'))

export const fact = (num: number): any => {
  if (Number.isInteger(num)) {
    if (num < 0) { return false }
    else if (num === 0) { return 1 }
    else {
      return (num * fact(num - 1))
    }
  }
  else return NaN
}
